'use strict';

const restify = require('restify');
const swaggerJSDoc = require('swagger-jsdoc');
const config = require('./config.json')[process.env.NODE_ENV || 'local'];
const winston = require('winston');
const os = require('os');

// Mandatory Configuration
const apiServiceName = 'hello-world';
const apiBasePath = '/hello-world';

// Create global logger object
const logger = new (winston.Logger)({
  transports: [
    new winston.transports.Console({
      level: 'info',
      timestamp: new Date().toString(),
      json: true,
    }),
  ],
});

// Create swaggerSpec object
const swaggerSpec = swaggerJSDoc(config.swagger);

// Create restify server object
const server = restify.createServer({
  name: apiServiceName,
});

server.use(restify.bodyParser({ mapParams: false })); // mapped in req.body

// Get IP Address of Server
function getIP() {
  return new Promise((resolve, reject) => {
    const adrs = os.networkInterfaces();
    let count = 0;
    Object.keys(adrs).forEach((key) => {
      adrs[key].forEach((item) => {
        if (item.internal === false && item.family === 'IPv4') {
          resolve(item.address);
          count += 1;
        }
      });
    });
    if (count === 0) {
      reject(`No IP found for ${apiServiceName}`);
    }
  });
}

/**
 * @swagger
 * definitions:
 *    schemas:
 *      type: object
 *      required:
 *      - name
 *      properties:
 *        name:
 *          type: string
 */

/**
 * @swagger
 * definitions:
 *   HelloWorldResponse:
 *    properties:
 *       hello:
 *         type: string
 * /hello-world:
 *   get:
 *     tags:
 *       - helloworld
 *     description: Returns hello world
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A JSON hello world
 *         schema:
 *           $ref: '#/definitions/HelloWorldResponse'
 */
server.get(apiBasePath, (req, res, next) => {
  const responsePayload = {
    hello: 'world',
  };
  res.send(200, responsePayload);
  next();
});

/**
 * @swagger
 * definitions:
 *   HelloWorldResponseWithId:
 *    properties:
 *       hello:
 *         type: string
 * /hello-world/test/{id}:
 *   get:
 *     tags:
 *       - helloworldwithid
 *     parameters:
 *       - in: path
 *         name: id
 *         type: string
 *         required: true
 *         description: Gets your specified entry
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A JSON hello world that also shows your sub-path input
 *         schema:
 *           $ref: '#/definitions/HelloWorldResponseWithId'
 */
server.get(`${apiBasePath}/test/:id`, (req, res, next) => {
  const subpath = req.params.id;
  const responsePayload = {
    hello: `your subpath input is ${subpath}`,
  };
  res.send(200, responsePayload);
  next();
});

/**
 * @swagger
 * definitions:
 *   HelloWorldResponsePost:
 *    properties:
 *       hello:
 *         type: string
 *       body:
 *         type: object
 * /hello-world:
 *   post:
 *     tags:
 *       - helloworldpost
 *     parameters:
 *       - in: body
 *         name: postBody
 *         schema:
 *           $ref: "#/definitions/schemas"
 *         required: true
 *         description: Body you want to post to request
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A JSON hello world that also shows your post body
 *         schema:
 *           $ref: '#/definitions/HelloWorldResponsePost'
 */
server.post(`${apiBasePath}`, (req, res, next) => {
  const postBody = req.body;
  const responsePayload = {
    hello: 'your post body is below',
    body: postBody,
  };
  res.send(200, responsePayload);
  next();
});

/**
 * @swagger
 * definitions:
 *   HelloWorldResponsePut:
 *    properties:
 *       hello:
 *         type: string
 *       body:
 *         type: object
 * /hello-world/test/{id}:
 *   put:
 *     tags:
 *       - helloworldput
 *     parameters:
 *       - in: body
 *         name: putBody
 *         schema:
 *           $ref: "#/definitions/schemas"
 *         required: true
 *         description: Body you want to submit to change
 *       - in: path
 *         name: id
 *         type: string
 *         required: true
 *         description: Record that you want to change
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A JSON hello world that also shows your post body
 *         schema:
 *           $ref: '#/definitions/HelloWorldResponsePut'
 */
server.put(`${apiBasePath}/test/:id`, (req, res, next) => {
  const putBody = req.body;
  const entry = req.params.id;
  const responsePayload = {
    hello: 'your put body is below',
    entry: `your editted entry is ${entry}`,
    body: putBody,
  };
  res.send(200, responsePayload);
  next();
});

/**
 * @swagger
 * definitions:
 *   HelloWorldResponseDelete:
 *    properties:
 *       hello:
 *         type: string
 *       entry:
 *         type: string
 * /hello-world/test/{id}:
 *   delete:
 *     tags:
 *       - helloworlddelete
 *     parameters:
 *       - in: path
 *         name: id
 *         type: string
 *         required: true
 *         description: Deletes your specified entry
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A JSON hello world that also shows your deleted entry record
 *         schema:
 *           $ref: '#/definitions/HelloWorldResponseDelete'
 */
server.del(`${apiBasePath}/test/:id`, (req, res, next) => {
  const entry = req.params.id;
  const responsePayload = {
    hello: 'your put body is below',
    entry: `your deleted entry is ${entry}`,
  };
  res.send(200, responsePayload);
  next();
});

/**
 * @swagger
 * definitions:
 *   HealthResponse:
 *    properties:
 *       status:
 *         type: string
 *       timestamp:
 *         type: string
 * /hello-world/health:
 *   get:
 *     tags:
 *       - health
 *     description: Returns the healthcheck status of API
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A JSON object indiciating the status of the API
 *         schema:
 *           $ref: '#/definitions/HealthResponse'
 */
server.get(`${apiBasePath}/health`, (req, res, next) => {
  const dateNow = new Date().toString();
  const responsePayload = {
    status: 'alive',
    timestamp: dateNow,
  };
  res.send(200, responsePayload);
  return next();
});

/**
 * @swagger
 * definitions:
 *   SwaggerResponse:
 *    properties:
 *       info:
 *         type: object
 *       paths:
 *         type: object
 *       definitions:
 *         type: object
 *       responses:
 *         type: object
 *       parameters:
 *         type: object
 *       securityDefinitions:
 *         type: object
 *       tags:
 *         type: object
 * /hello-world/swagger:
 *   get:
 *     tags:
 *       - swagger
 *     description: Returns a swagger definition in JSON format
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A swagger definition object in JSON
 *         schema:
 *           $ref: '#/definitions/SwaggerResponse'
 */
server.get(`${apiBasePath}/swagger`, (req, res, next) => {
  res.send(swaggerSpec);
  return next();
});

server.listen(5000, () => {
  getIP().then((ip) => {
    logger.info(`Micro Service Running on Port: ${ip}:${server.address().port}`);
  }).catch((error) => {
    logger.error(error);
  });
});
